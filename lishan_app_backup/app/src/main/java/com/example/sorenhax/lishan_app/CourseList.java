package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by sorenhax on 13-02-2018.
 */


@Entity(tableName="courselist",indices = {@Index(value= {"lesid","courseid"},unique = true),@Index(value="listId",unique = true)})
public class CourseList {
    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public double getLesid() {
        return lesid;
    }

    public void setLesid(double lesid) {
        this.lesid = lesid;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    public String getListname() {
        return listname;
    }

    public void setListname(String listname) {
        this.listname = listname;
    }

    public long getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(long insertTime) {
        this.insertTime = insertTime;
    }


    @PrimaryKey(autoGenerate = true)
    private int listId;


    private double lesid;
    private int courseid;

    @ColumnInfo
    private String listname;

    @ColumnInfo
    private long insertTime; //UnixTime

    public CourseList(int courseid,double lesid,String listname) {
        this.lesid=lesid;
        this.listname=listname;
        this.courseid=courseid;
        this.insertTime=System.currentTimeMillis() / 1000L;
    }
}
