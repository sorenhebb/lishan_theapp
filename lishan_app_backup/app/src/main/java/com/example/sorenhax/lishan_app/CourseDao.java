package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sorenhax on 06-02-2018.
 */
@Dao
public interface CourseDao {
        @Query("SELECT * FROM courses")
        List<Course> getAll();

        @Query("SELECT * FROM courses WHERE courseid IN (:courseid)")
        List<Course> loadById(int courseid);

        @Insert
        void insert(Course courses);

        @Delete
        void delete(Course course);
}
