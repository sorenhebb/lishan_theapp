package com.example.sorenhax.lishan_app;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sorenhax on 22-01-2018.
 */

public class QueryLine implements Parcelable{
    String bool;
    String not;
    String lprts;
    String p;
    String t;
    String v;
    String rprts;

    public QueryLine(String bool, String not, String lprts, String p, String t, String v, String rprts) {
        this.bool = bool;
        this.not = not;
        this.lprts = lprts;
        this.p = p;
        this.t = t;
        this.v = v;
        this.rprts = rprts;
    }

    protected QueryLine(Parcel in) {
        bool = in.readString();
        not = in.readString();
        lprts = in.readString();
        p = in.readString();
        t = in.readString();
        v = in.readString();
        rprts = in.readString();
    }

    public static final Creator<QueryLine> CREATOR = new Creator<QueryLine>() {
        @Override
        public QueryLine createFromParcel(Parcel in) {
            return new QueryLine(in);
        }

        @Override
        public QueryLine[] newArray(int size) {
            return new QueryLine[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(bool);
        parcel.writeString(not);
        parcel.writeString(lprts);
        parcel.writeString(p);
        parcel.writeString(t);
        parcel.writeString(v);
        parcel.writeString(rprts);
    }

    public String getBool() {
        return bool;
    }

    public void setBool(String bool) {
        this.bool = bool;
    }

    public String getNot() {
        return not;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getLprts() {
        return lprts;
    }

    public void setLprts(String lprts) {
        this.lprts = lprts;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getRprts() {
        return rprts;
    }

    public void setRprts(String rprts) {
        this.rprts = rprts;
    }

}
