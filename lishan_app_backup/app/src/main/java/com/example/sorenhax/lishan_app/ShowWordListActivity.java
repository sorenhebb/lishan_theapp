package com.example.sorenhax.lishan_app;

import android.app.ListActivity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowWordListActivity extends ListActivity {

    ArrayList<WordItem> alist=new ArrayList<>();
    String [] cols;
    int courseNumber;
    String courseLanguage;
    String queryType;
    String querylinesJ;
    String courseInfo;
    String mainTag;

    static AsyncTask saveTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordlistlayout);
        SharedPreferences loginInfo=getSharedPreferences("user_login",MODE_PRIVATE);
        String username=loginInfo.getString("username","none");
        String password=loginInfo.getString("password","none");
        if (username=="none" || password=="none") {
            Intent intent = new Intent(this, LoginActivity2.class);
            startActivity(intent);
        }

        Bundle extras=getIntent().getExtras();

        ArrayList<QueryLine> queryLines=extras.getParcelableArrayList(MainActivity.QUERY);
        querylinesJ=new Gson().toJson(queryLines);
        courseInfo=extras.getString("courseInfo");
        queryType=extras.getString("queryType");
        mainTag=extras.getString("mainTag");
        try {
            JSONObject courseJson=new JSONObject(courseInfo);
            cols=courseJson.getString("maincols").split(",");
            courseNumber=courseJson.getInt("id");
            courseLanguage=courseJson.getString("betegnelse");
            //Toast.makeText(ShowWordListActivity.this,cols,Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (queryType.equals("CourseList")) displayDBWordList(Double.parseDouble(mainTag));
        else getWordListFromServer(querylinesJ);
    }

    private void getWordListFromServer(String jsonString) {
        String url ="http://lishan.dk/lishan2/index.php/appcontroller/simplequery/"+courseNumber;
        SharedPreferences sharedPref = getSharedPreferences("user_login",MODE_PRIVATE);
        String username=sharedPref.getString("username","anonymous");
        String password=sharedPref.getString("password","anonymous");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("q", jsonString);
        JSONObject jsonObj = new JSONObject(params);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,url,jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String message= null;
                        try {
                            message = response.getString("message");
                            //Toast.makeText(ShowWordListActivity.this,message,Toast.LENGTH_LONG).show();
                            displayWordList(response.getJSONObject("result"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.topView),""+error,Snackbar.LENGTH_LONG);
                mySnackbar.show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    private void displayWordList(JSONObject jsonObject) {
        Log.i("Query From","server");
        try {
            JSONObject own=jsonObject;
            for(int i=0;i<own.length();i++) {
                JSONObject obj=own.getJSONObject(String.valueOf(i));
                WordItem w=new WordItem(obj,cols,courseLanguage);
                alist.add(w);
            }
            setadapter();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void displayDBWordList(double lesid) {
        Log.i("Query From","DB");

        new AsyncTask<Double,Void,Void>() {
            @Override
            protected Void doInBackground(Double... params) {
                LishanDatabase db = Room.databaseBuilder(getApplicationContext(), LishanDatabase.class, "lishan_dk").fallbackToDestructiveMigration().build();
                int listId=db.CourseListDao().loadById(courseNumber,params[0]).get(0).getListId();
                double lesid=db.CourseListDao().loadById(courseNumber,params[0]).get(0).getLesid();
                List<LemmaMSA> own=db.ListLemmaJoinDao().loadByListId(listId);


                Log.i("Size of list",String.valueOf(own.size()));
                for(int i=0;i<own.size();i++) {
                    LemmaMSA obj=own.get(i);
                    WordItem w=new WordItem(obj);
                    w.listId=listId;
                    w.lesid=lesid;
                    w.courseId=courseNumber;
                    alist.add(w);
                }
            return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                super.onPostExecute(v);
                setadapter();
            }
        }.execute(lesid);

    }

    public void setadapter() {
        WordListAdapter adapter= new WordListAdapter(this, alist);
        ListView listView = (ListView) getListView();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence txt=alist.get(i).name;
                Log.i("Listid",String.valueOf(alist.get(i).listId));
                Log.i("Lesid",String.valueOf(alist.get(i).lesid));
                Log.i("Courseid",String.valueOf(alist.get(i).courseId));
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.topView),txt,Snackbar.LENGTH_LONG);
                mySnackbar.show();
            }
        });
        //adapter.addAll(alist);
        }
    }
