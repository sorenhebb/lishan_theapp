package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.PendingIntent.getActivity;
import static android.content.Context.MODE_PRIVATE;


/**
 * Created by sorenhax on 04-01-2018.
 */

public class ListAdapter extends ArrayAdapter<WordListItem> {

    public ListAdapter(Context context, ArrayList<WordListItem> listItem) {
        super(context, 0, listItem);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        WordListItem list = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_layout, parent, false);
        }
        // Lookup view for data population
        TextView listName = (TextView) convertView.findViewById(R.id.listname);
        // Populate the data into the template view using the data object
        String t = list.name;
        String p =list.comment;
        if (!list.comment.equals("")) t += "\n"+p;
        if (list.downloaded == true) t = "*"+t;
        listName.setText(t);
        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
}
