package com.example.sorenhax.lishan_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sorenhax on 14-01-2018.
 */

public class WordListAdapter extends ArrayAdapter<WordItem>{

    public WordListAdapter(Context context, ArrayList<WordItem> word) {
        super(context, 0, word);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        WordItem word = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_layout, parent, false);
        }
        // Lookup view for data population
        TextView listName = (TextView) convertView.findViewById(R.id.listname);
        // Populate the data into the template view using the data object
        JSONObject content=word.content;
        String txt=word.name;
        if (!word.morecolsA1.equals("")) txt+=" "+word.morecolsA1;
        if (!word.morecolsA2.equals("")) txt+=" "+word.morecolsA2;
        if (!word.altname.equals("")) txt+="\n"+word.altname;
        if (!word.morecolsB1.equals("")) txt+=" "+word.morecolsB1;
        if (!word.morecolsB2.equals("")) txt+=" "+word.morecolsB2;
        txt+="\n"+word.definition;
        listName.setText(txt);
        return convertView;
    }
}

