package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sorenhax on 13-02-2018.
 */

@Dao
public interface CourseListDao {
    @Query("SELECT * FROM courselist WHERE courseid=:courseid")
    List<CourseList> getAll(int courseid);

    @Query("SELECT * FROM courselist WHERE courseid =:courseid AND lesid in (:lesid)")
    List<CourseList> loadById(int courseid, double lesid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(CourseList courselist);

    @Delete
    void delete(CourseList courseList);
}
