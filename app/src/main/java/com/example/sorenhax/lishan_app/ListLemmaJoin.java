package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

/**
 * Created by sorenhax on 13-02-2018.
 */

@Entity(tableName = "list_lemma_join",
        primaryKeys = { "listId", "lemmaId" },
        indices = @Index(value={"lemmaId","listId"},unique = true))
public class ListLemmaJoin {
        private long listId;
        private long lemmaId;

        public ListLemmaJoin(long listId,long lemmaId){
                this.listId=listId;
                this.lemmaId=lemmaId;
        }

        public long getListId() {
                return listId;
        }

        public void setListId(int listId) {
                this.listId = listId;
        }

        public long getLemmaId() {
                return lemmaId;
        }

        public void setLemmaId(int lemmaId) {
                this.lemmaId = lemmaId;
        }
}
