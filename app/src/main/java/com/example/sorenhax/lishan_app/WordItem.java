package com.example.sorenhax.lishan_app;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by sorenhax on 13-01-2018.
 */

public class WordItem {
    public Integer id;
    public String name="";
    public JSONObject content;
    public String definition="";
    public String altname="";
    public String morecolsA1="";
    public String morecolsA2="";
    public String morecolsB1="";
    public String morecolsB2="";
    public int listId;
    public double lesid;
    public int courseId;
    public WordItem(JSONObject wordobj,String[]colinfo,String language) {
        String definitionCol=colinfo[0];
        String nameCol=colinfo[1];
        this.content=wordobj;
        try {
            switch(language) {
                case "MSA":
                    this.definition=wordobj.getString("dansk");
                    this.id=wordobj.getInt("id");
                    this.name=wordobj.getString("singperf");
                    this.altname=wordobj.getString("transsingperf");
                    this.morecolsA1=wordobj.getString("plurimp");
                    this.morecolsA2=wordobj.getString("masdar");
                    this.morecolsB1=wordobj.getString("transplurimp");
                    this.morecolsB2=wordobj.getString("transmasdar");
                    break;
                case "EA":
                    this.definition=wordobj.getString("dk");
                    this.id=wordobj.getInt("id");
                    this.name=wordobj.getString("eatr");
                    this.altname=wordobj.getString("eaar");
                    this.morecolsA1=wordobj.getString("ea2tr");
                    this.morecolsB1=wordobj.getString("ea2ar");
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public WordItem(JSONObject wordobj,String[]colinfo) {
        String definitionCol=colinfo[0];
        String nameCol=colinfo[1];
        try {
                    this.definition=wordobj.getString(definitionCol);
                    this.id=wordobj.getInt("id");
                    this.name=wordobj.getString(nameCol);
                    this.content=wordobj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public WordItem(LemmaMSA lemmaMSA) {
        this.definition=lemmaMSA.getSingperf();
        this.name=lemmaMSA.getDansk();
        this.id=lemmaMSA.getId();
        this.listId=lemmaMSA.getListId();
        this.lesid=lemmaMSA.getLesid();
        this.courseId=lemmaMSA.getCourseId();
    }

}
