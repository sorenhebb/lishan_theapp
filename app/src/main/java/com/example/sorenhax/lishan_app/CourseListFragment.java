package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.sorenhax.lishan_app.LishanDatabase.MIGRATION_3_5;

/**
 * Created by sorenhax on 15-01-2018.
 */

public class CourseListFragment extends ListFragment{
    ArrayList<WordListItem> courseArrayList;
    LishanDatabase db;
    Context sContext;
    String language;
    int outscroll;
    int clickScroll;
    static int courseId;
    static SharedPreferences sharedPref;
    static ArrayList<CourseList> toBeDownloadedAndSaved;
    static ArrayList<CourseList> toBeDeleted;
    static ListAdapter adpt;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.courselistlayout,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sContext=getContext();
        courseId=getArguments().getInt("faveCourse");
        language=getArguments().getString("languageCode");
        db=Room.databaseBuilder(getActivity(), LishanDatabase.class, "lishan_dk").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        sharedPref = this.getActivity().getSharedPreferences("user_login",MODE_PRIVATE);
        if (courseArrayList == null) {
            courseArrayList=new ArrayList<>();
            populateListsFromServer();
        }
        else {
            Log.i("LISTSIZE","LArge");
            setadapter();
        }


    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (clickScroll!=0) outscroll=clickScroll;
        else outscroll=getListView().getSelectedItemPosition();
        outState.putInt("scroll", outscroll);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        if (savedInstanceState != null)
            outscroll = savedInstanceState.getInt("scroll");
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("POS","onListItemClick");
        clickScroll=position;
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getActivity().getApplicationContext(), ShowWordListActivity.class);
        ArrayList<QueryLine> arl=new ArrayList<QueryLine>();
        QueryLine line=new QueryLine("","","","lesid","=",courseArrayList.get(position).name,"");
        arl.add(line);
        intent.putParcelableArrayListExtra(MainActivity.QUERY,arl);
        intent.putExtra("courseInfo",getArguments().getString("courseinfo"));
        intent.putExtra("queryType","CourseList");
        intent.putExtra("mainTag",courseArrayList.get(position).name);
        startActivity(intent);
    }

    public void populateListsFromServer() {
        Log.i("POS","populatelistsfromserver");
        String url ="http://lishan.dk/lishan2/index.php/appcontroller/listOfLists/"+getArguments().getInt("faveCourse");
        // Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        String username=getArguments().getString("username");
        String password=getArguments().getString("password");
        params.put("username", username);
        params.put("password", password);
        // Instantiate the RequestQueue.
        JSONObject jsonObj = new JSONObject(params);
        Log.i("JSON_CONTENT",params.toString()+url);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.
        JsonObjectRequest jsonRequest =
                new JsonObjectRequest(Request.Method.POST,url,jsonObj,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("Debug", response.toString());
                                fillListWithJSON(response);
                                if (toBeDownloadedAndSaved.size()>0)
                                     queueListForSaveToDisc();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                MainActivity.displayError("Offline Mode");
                                VolleyLog.d("", "Error: " + error.getMessage());
                                Log.i("JSON_ERROR_POPULATE",""+error);
                            }
                        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    public void fillListWithJSON(JSONObject jsonObject) {
        Log.i("POS","fillListWithJSON");
        toBeDownloadedAndSaved=new ArrayList<CourseList>();
        toBeDeleted=new ArrayList<CourseList>();
        try {
            JSONArray course=jsonObject.getJSONArray("course");
            int dbListCounter=0;
            List <CourseList> dblists=db.CourseListDao().getAll(courseId);
            for(int i=0;i<course.length();i++) {
                String lesid=course.getJSONObject(i).getString("lesid");
                //String feedback="Lesid:"+String.valueOf(lesid)+",i:"+String.valueOf(i)+"dblistsize:"+String.valueOf(dblists.size());
                //Log.i("Feedback:",feedback);
                //
                String listname=course.getJSONObject(i).getString("olbetegnelse");
                WordListItem w=new WordListItem(lesid ,listname,false);
                double serverles=Double.parseDouble(lesid);
                CourseList newcl=new CourseList(courseId,serverles,listname);
                if (dbListCounter >= dblists.size()) {
                    toBeDownloadedAndSaved.add(newcl);
                    courseArrayList.add(w);
                    Log.i("AddingA","n"+newcl.getListname());
                    continue;
                }
                double dbles=dblists.get(dbListCounter).getLesid();
                if (dbles==serverles) {
                    w.downloaded=true;
                    dbListCounter++;
                }
                else if (dbles>serverles)  {
                    toBeDownloadedAndSaved.add(newcl);
                    Log.i("AddingB",newcl.getListname());
                }
                else while (serverles>dbles) {
                    toBeDeleted.add(newcl);
                    dbListCounter++;
                    dbles=dblists.get(dbListCounter).getLesid();
                }
                courseArrayList.add(w);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        setadapter();
        if (outscroll !=0) getListView().setSelection(outscroll);
    }

    private void setadapter() {
        ListAdapter adapter = new ListAdapter(getContext(), courseArrayList);
        ListView listView = (ListView) getListView();
        listView.setAdapter(adapter);

    }

    public void populateListsFromDB() {
        Log.i("POS","populateListsFromDB");
        List<CourseList> clist=db.CourseListDao().getAll(getArguments().getInt("faveCourse"));
        fillListWithCourseListList(clist);
       }

    private void fillListWithCourseListList(List<CourseList> clist) {
        Log.i("Sizeof DB",String.valueOf(clist.size()));
        for (CourseList cl:clist) {
            String lesid=String.valueOf(cl.getLesid());
            String listname=cl.getListname();
            WordListItem w=new WordListItem(lesid,listname,false);
            courseArrayList.add(w);
        }
        adpt=new ListAdapter(sContext,courseArrayList);
        setListAdapter(adpt);
        if (outscroll !=0) getListView().setSelection(outscroll);
    }

    private void queueListForSaveToDisc() {

        Log.i("POS","queueListForSaveToDisc");
        CourseList list;
        final String lesid;
        if(toBeDownloadedAndSaved.size()>0) {
            list = toBeDownloadedAndSaved.remove(0);
            lesid = String.valueOf(list.getLesid());
            Log.i("Still_to_be_DLd",String.valueOf(toBeDownloadedAndSaved.size())+"lesid:"+lesid);
        }
        else return;
        ArrayList<QueryLine> queryLines=new ArrayList<QueryLine>();
        QueryLine qline=new QueryLine("","","","lesid","=",lesid,"");
        queryLines.add(qline);
        String queryLinesJ=new Gson().toJson(queryLines);
        String url ="http://lishan.dk/lishan2/index.php/appcontroller/simplequery/"+courseId;
        String username=sharedPref.getString("username","anonymous");
        String password=sharedPref.getString("password","anonymous");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("q", queryLinesJ);
        JSONObject jsonObj = new JSONObject(params);
        RequestQueue queue = Volley.newRequestQueue(sContext);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,url,jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("MESSAGE",response.getString("message"));
                            JSONObject rows=response.getJSONObject("rows");
                            JSONObject listmeta=response.getJSONObject("listMeta");
                            Double lesid = Double.parseDouble(listmeta.getString("lesid"));
                            String listName=listmeta.getString("olbetegnelse");
                            CourseList cl=new CourseList(courseId,lesid,listName);
                            long listInsert=db.CourseListDao().insert(cl);
                            Log.i("Now adding",Double.toString(cl.getLesid()));
                            for (int i = 0; i < rows.length(); i++) {
                                String row;
                                 row = rows.getJSONObject(String.valueOf(i)).toString();
                                Gson gson = new Gson();
                                switch (language) {
                                    case "MSA":
                                        LemmaMSA lemma = gson.fromJson(row, LemmaMSA.class);
                                        long ins = db.LemmaMSADao().insert(lemma);
                                        ListLemmaJoin listLemmaJoin=new ListLemmaJoin(listInsert,ins);
                                        db.ListLemmaJoinDao().insert(listLemmaJoin);
                                        courseArrayList.get(i).downloaded=true;

                                        //String name=adpt.getItem(i).getName();
                                        //adpt.getItem(i).setName(name+"*");
                                }

                            }
                            if (toBeDownloadedAndSaved.size()>0) {
                                queueListForSaveToDisc();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar mySnackbar = Snackbar.make(MainActivity.mainContent,"YY"+error,Snackbar.LENGTH_LONG);
                mySnackbar.show();
                Log.i("ERROR",error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }
}
