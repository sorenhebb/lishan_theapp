package com.example.sorenhax.lishan_app;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;

/**
 * Created by sorenhax on 06-02-2018.
 */
@Database(entities = {Course.class,LemmaMSA.class,CourseList.class,ListLemmaJoin.class}, version = 6)
public abstract class LishanDatabase extends RoomDatabase {
        public abstract CourseDao CourseDao();
        public abstract LemmaMSADao LemmaMSADao();
        public abstract CourseListDao CourseListDao();
        public abstract ListLemmaJoinDao ListLemmaJoinDao();

        static final Migration MIGRATION_3_5 = new Migration(3, 5) {
                @Override
                public void migrate(SupportSQLiteDatabase database) {
                        database.execSQL("ALTER TABLE CourseList "
                                + " ADD COLUMN insertTime LONG");

                }
        };
}


