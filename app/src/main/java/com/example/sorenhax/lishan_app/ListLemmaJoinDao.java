package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sorenhax on 13-02-2018.
 */
@Dao
public interface ListLemmaJoinDao {

    @Insert
    void insert(ListLemmaJoin listLemmaJoin);

    @Query("SELECT * FROM list_lemma_join")
    List<ListLemmaJoin> getAll();

    @Query("SELECT MSAlemmas.* FROM MSAlemmas INNER JOIN list_lemma_join ON MSAlemmas.id=lemmaId INNER JOIN courselist ON courselist.listId=list_lemma_join.listId WHERE lesid IN (:lesid) AND courseid = :courseid")
    List<LemmaMSA> loadByLesid(int courseid,double lesid);

    @Query("SELECT MSAlemmas.*,list_lemma_join.listId FROM MSAlemmas INNER JOIN list_lemma_join ON MSAlemmas.id=list_lemma_join.lemmaId WHERE list_lemma_join.listId =:listId")
    List<LemmaMSA> loadByListId(final int listId);

    @Query("SELECT * FROM MSAlemmas INNER JOIN list_lemma_join ON MSAlemmas.id=list_lemma_join.lemmaId")
    List<LemmaMSA> loadAllLists();


}
