package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sorenhax on 06-02-2018.
 */

@Entity(tableName = "courses")
public interface LemmaDao {
    @Query("SELECT * FROM lemmas")
    List<Lemma> getById();

    @Query("SELECT * FROM lemmas WHERE courseid IN (:courseid)")
    List<Course> loadById(int courseid);

    @Insert
    void insert(Course courses);

    @Delete
    void delete(Course course);
}
