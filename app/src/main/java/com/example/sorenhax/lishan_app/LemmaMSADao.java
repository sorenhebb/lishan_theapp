package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sorenhax on 12-02-2018.
 */

@Dao
public interface LemmaMSADao {
    @Query("SELECT * FROM MSAlemmas")
    List<LemmaMSA> getAll();

    @Query("SELECT * FROM MSAlemmas WHERE id IN (:id)")
    List<LemmaMSA> loadById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(LemmaMSA lemma);

    @Delete
    void delete(LemmaMSA lemma);
}