package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Created by sorenhax on 12-02-2018.
 */
@Entity(tableName = "MSAlemmas")
public class LemmaMSA {

        public LemmaMSA() {

        }
        @PrimaryKey
        private int id;
        @ColumnInfo
        private String singperf;
        @ColumnInfo
        private String plurimp;
        @ColumnInfo
        private String masdar;
        @ColumnInfo
        private String dansk;
        @ColumnInfo
        private String novoksingperf;
        @ColumnInfo
        private String novokplurimp;
        @ColumnInfo
        private String novokmasdar;
        @ColumnInfo
        private String lemmasp;
        @ColumnInfo
        private String lemmapi;
        @ColumnInfo
        private String lemmamas;
        @ColumnInfo
        private String ordklasse;
        @ColumnInfo
        private String ordklassedk;
        @ColumnInfo
        private int frekvensid;
        @ColumnInfo
        private int stamform;
        @Ignore
        private int listId;
        @Ignore
        private double lesid;
        @Ignore
        private int courseId;

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public double getLesid() {
        return lesid;
    }

    public void setLesid(double lesid) {
        this.lesid = lesid;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSingperf() {
        return singperf;
    }

    public void setSingperf(String singperf) {
        this.singperf = singperf;
    }

    public String getPlurimp() {
        return plurimp;
    }

    public void setPlurimp(String plurimp) {
        this.plurimp = plurimp;
    }

    public String getMasdar() {
        return masdar;
    }

    public void setMasdar(String masdar) {
        this.masdar = masdar;
    }

    public String getDansk() {
        return dansk;
    }

    public void setDansk(String dansk) {
        this.dansk = dansk;
    }

    public String getNovoksingperf() {
        return novoksingperf;
    }

    public void setNovoksingperf(String novoksingperf) {
        this.novoksingperf = novoksingperf;
    }

    public String getNovokplurimp() {
        return novokplurimp;
    }

    public void setNovokplurimp(String novokplurimp) {
        this.novokplurimp = novokplurimp;
    }

    public String getNovokmasdar() {
        return novokmasdar;
    }

    public void setNovokmasdar(String novokmasdar) {
        this.novokmasdar = novokmasdar;
    }

    public String getLemmasp() {
        return lemmasp;
    }

    public void setLemmasp(String lemmasp) {
        this.lemmasp = lemmasp;
    }

    public String getLemmapi() {
        return lemmapi;
    }

    public void setLemmapi(String lemmapi) {
        this.lemmapi = lemmapi;
    }

    public String getLemmamas() {
        return lemmamas;
    }

    public void setLemmamas(String lemmamas) {
        this.lemmamas = lemmamas;
    }

    public String getOrdklasse() {
        return ordklasse;
    }

    public void setOrdklasse(String ordklasse) {
        this.ordklasse = ordklasse;
    }

    public String getOrdklassedk() {
        return ordklassedk;
    }

    public void setOrdklassedk(String ordklassedk) {
        this.ordklassedk = ordklassedk;
    }

    public int getFrekvensid() {
        return frekvensid;
    }

    public void setFrekvensid(int frekvensid) {
        this.frekvensid = frekvensid;
    }

    public int getStamform() {
        return stamform;
    }

    public void setStamform(int stamform) {
        this.stamform = stamform;
    }
}
