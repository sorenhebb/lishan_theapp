package com.example.sorenhax.lishan_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * Created by sorenhax on 15-01-2018.
 */

public class OwnListFragment extends android.support.v4.app.ListFragment{

    ArrayList<WordListItem> ownArrayList=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.ownlistlayout,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateListsFromServer();
    }

    public void fillList(JSONObject jsonObject) {

        try {
            JSONArray own=jsonObject.getJSONArray("own");
            for(int i=0;i<own.length();i++) {

                WordListItem w=new WordListItem(own.getJSONObject(i).getString("wordtag"),"",(i<4) );
                ownArrayList.add(w);
            }

            ListAdapter adpt=new ListAdapter(getActivity(),ownArrayList);
            setListAdapter(adpt);
            /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(MainActivity.this,ownArrayList.get(i).name,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, ShowWordListActivity.class);
                    intent.putExtra(EXTRA_MESSAGE, alist.get(i).name);
                    startActivity(intent);
                }
            });*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getActivity().getApplicationContext(), ShowWordListActivity.class);
        ArrayList<QueryLine> arl=new ArrayList<QueryLine>();
        QueryLine line=new QueryLine("","","","wordtag","=",ownArrayList.get(position).name,"");
        arl.add(line);
        intent.putParcelableArrayListExtra(MainActivity.QUERY,arl);

        intent.putExtra("courseinfo",getArguments().getString("courseinfo"));
        startActivity(intent);
    }

    public void populateListsFromServer() {

        String url ="http://lishan.dk/lishan2/index.php/appcontroller/listOfLists/"+getArguments().getInt("faveCourse");

        // Post params to be sent to the server
        /*

        if (username.equals("anonymous")) {
            Snackbar mySnackbar = Snackbar.make(getView(),",mn,mn",Snackbar.LENGTH_SHORT);
            mySnackbar.show();
//Intent login screen
        }
        else {
            Snackbar mySnackbar = Snackbar.make(getView(),username,Snackbar.LENGTH_SHORT);
            mySnackbar.show();
        }

        Intent intent = new Intent(this, LoginActivity2.class);
        startActivity(intent);
        */
        String username=getArguments().getString("username");
        String password=getArguments().getString("password");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);

        // Instantiate the RequestQueue.
        JSONObject jsonObj = new JSONObject(params);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        // Request a string response from the provided URL.

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,url,jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        fillList(response);
                        //Snackbar mySnackbar = Snackbar.make(getView(),"Korrekt login i ownlist",Snackbar.LENGTH_SHORT);
                        //mySnackbar.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Snackbar mySnackbar = Snackbar.make(getView(),"Forkert login i ownlist",Snackbar.LENGTH_SHORT);
                //mySnackbar.show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

}
