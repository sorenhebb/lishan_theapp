package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by sorenhax on 06-02-2018.
 */
@Entity(tableName = "courses")
public class Course {
            @PrimaryKey
        private int courseid;

        @ColumnInfo(name = "language")
        private String language;

    @ColumnInfo(name = "coursename")
    private String coursename;

    @ColumnInfo(name = "maincols")
    private String maincolumns;

    public Course(int courseid, String language, String coursename, String maincolumns) {
        this.courseid = courseid;
        this.language = language;
        this.coursename = coursename;
        this.maincolumns = maincolumns;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public String getMaincolumns() {
        return maincolumns;
    }

    public void setMaincolumns(String maincolumns) {
        this.maincolumns = maincolumns;
    }
}

