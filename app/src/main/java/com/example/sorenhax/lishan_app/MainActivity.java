package com.example.sorenhax.lishan_app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    public SectionsPagerAdapter mSectionsPagerAdapter;
    public static final String QUERY = "com.lishan.QUERY";
    public static CoordinatorLayout mainContent;
    Spinner spinner;
    Integer [] courseids;
    static Context sContext;
    String languageCode;
    String username;
    String password;
      /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public Bundle prefs;
    public Course c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sContext=getApplicationContext();
        setContentView(R.layout.activity_main);
        SharedPreferences loginInfo=getSharedPreferences("user_login",MODE_PRIVATE);
        username=loginInfo.getString("username","none");
        password=loginInfo.getString("password","none");
        if (username.equals("none") || password.equals("none")) {
            goToLogin();
        }
        mainContent=findViewById(R.id.main_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        prefs=new Bundle();
        prefs.putString("username",username);
        prefs.putString("password",password);
        spinner= (Spinner) findViewById(R.id.courseSpinner);
        String courselist=getSharedPreferences("user_settings",MODE_PRIVATE).getString("courselist","");
        if (courselist.equals(""))
            goToLogin();
        populateCourseSpinner(courselist);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Integer newc=Integer.parseInt(adapterView.getItemAtPosition(i).toString());
                prefs.putInt("faveCourse",newc);
                getCourseInfo(newc);
                prefs.putString("courseinfo",getCourseInfo(newc));
                prefs.putString("languageCode",languageCode);
                SharedPreferences.Editor editor = getSharedPreferences("user_settings", MODE_PRIVATE).edit();
                editor.putInt("faveCourse",newc);
                editor.apply();
                mSectionsPagerAdapter.notifyDataSetChanged();
                //Snackbar.make(findViewById(R.id.container),"abcde",Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        //else Snackbar.make(findViewById(R.id.container),"logget ind som "+username,Snackbar.LENGTH_SHORT).show();
        //mSectionsPagerAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_login:
                goToLogin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity2.class);
        startActivity(intent);
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    CourseListFragment frag1=new CourseListFragment();
                    frag1.setArguments(prefs);
                    return frag1;
                case 1:
                    OwnListFragment frag2=new OwnListFragment();
                    frag2.setArguments(prefs);
                    return frag2;
                case 2:
                    Tab3 frag3=new Tab3();
                    return frag3;
            }
            return null;
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    public String getCourseInfo(Integer coursenr) {
        String courselist=getSharedPreferences("user_settings",MODE_PRIVATE).getString("courselist","");

        try {
            JSONArray obj = new JSONArray(courselist);

            int pos=Arrays.asList(courseids).indexOf(coursenr);
            languageCode=obj.getJSONObject(pos).getString("betegnelse");
            return obj.getJSONObject(pos).toString();

        } catch (Throwable t) {
        }
        return "";
    }


public void populateCourseSpinner(String jsonObj) {
    Integer faveCourse = getSharedPreferences("user_settings",MODE_PRIVATE).getInt("faveCourse",-1);

    try {
        JSONArray obj = new JSONArray(jsonObj);
        courseids=new Integer[obj.length()];
        for (Integer i=0;i<obj.length();i++) {
            JSONObject c=obj.getJSONObject(i);
            Integer courseid=c.getInt("courseid");
            courseids[i]=courseid;
        }

        ArrayAdapter<Integer> adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,courseids);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);


        if (faveCourse != null) {
            int spinnerPosition = adapter.getPosition(faveCourse);

            spinner.setSelection(spinnerPosition,false);
        }

    } catch (JSONException e) {
        e.printStackTrace();
    }
}

public static void displayError(String msg) {
    Snackbar.make(mainContent,msg,Snackbar.LENGTH_LONG).show();
}
}
