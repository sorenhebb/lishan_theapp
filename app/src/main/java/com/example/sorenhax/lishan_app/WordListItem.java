package com.example.sorenhax.lishan_app;

/**
 * Created by sorenhax on 04-01-2018.
 */

public class WordListItem {
    public String name;
    public boolean downloaded;
    public String comment;

    public WordListItem(String name,String comment, Boolean downloaded) {
        this.comment=comment;
        this.name=name;
        this.downloaded=downloaded;
    }

    public void setName(String name) {
        this.name=name;
    }

    public String getName() {
        return this.name;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
